# What is Maven?

[Apache Maven](http://maven.apache.org) is a software project management and comprehension tool. Based on the concept of a project
object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information.

# What is the purpose of this repository

This repository contains Docker descriptors to create images based on [official Maven images](https://hub.docker.com/_/maven/)
but with Maven running under a pre-created non-root user with home directory in `/home/user` and Maven configuration
in `/home/user/.m2`.